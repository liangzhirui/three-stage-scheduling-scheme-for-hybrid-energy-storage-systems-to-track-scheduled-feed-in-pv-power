main_program:Scheduling program of the three-stage scheduling scheme 
for hybrid energy storage systems to track scheduled feed-in PV power

Program used in expected service life of batteries computing
rfhist
sig2ext
rainflow

Data used in PV power computing
rhour
rminute
thour
tminute