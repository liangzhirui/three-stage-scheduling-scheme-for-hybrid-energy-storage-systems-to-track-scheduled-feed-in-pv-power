%% Three-stage scheduling scheme for hybrid energy storage systems to track scheduled feed-in PV power

% Choose the studied date 
Month=7;
Day=22;
%% %%%%%%%%%%%%%%%%%%    system parameter setting    %%%%%%%%%%%%%%%%%%%%
%Average daily hydrogen production   Unit: Cubic metres
Havr=200*10^4/365;
%Electricity Consumption to Produce One Cubic Meter of Hydrogen   Unit:kW.h/m^3
Qh1=4.6;
%hydrogen price   Unit:yuan/cubic metre
price_H=8;
%max input power of an electrolyzer   Unit:kW
PHmax=9184;
%min input power of an electrolyzer   Unit:kW
PHmin=1377;
%Rated Power of Batteries   Unit:kW
PBnormal=1500;
%Maximum charge-discharge rate of battery
Kmax=2;
%Maximum capacity of battery Unit��kWh
QBmax=3000;
%Initial SOC of battery
SOC0=0.75;
%Battery charging efficiency
KB1=0.949;
%Battery discharging efficiency
KB2=0.949;
%upper bound of SOC
SOCmax1=0.85;
%lower bound of SOC
SOCmin1=0.15;
%SOC threshold for exiting scheme ��
SOCmin2=0.20;
%On-peak price   Unit:yuan
price_peak=0.8769;
%Mid-peak price   Unit:yuan
price_mean=0.5502;
%Off-peak price   Unit:yuan
price_valley=0.2235;
%Distributed Photovoltaic Subsidy  Unit:yuan/kWh
subsidy=0.37;
%Selling Price of Centralized PV Power    Unit:yuan
price_cen=0.75;
%Selling Price of Distributed PV Power     Unit:yuan
price_dis=0.3545;
%Centralized PV power    Unit:kW
Qcen=20000;
%Distributed PV power    Unit:kW
Qdis=0;
%rated output power of a PV system   Unit:kW
Qpv=20000;
%standard for clear day
sunday=0.01;
square=0.96;
%% %%%%%%%%%%%%%%%%%%   Input the PV forecast data   %%%%%%%%%%%%%%%%%%%%
%Extraction of Hourly Radiation and Temperature Data
load rhour;
load thour;
rh=zeros(24,365);
th=zeros(24,365);
for i=1:365
    k1=(i-1)*24+1;
    k2=i*24;
    rh(:,i)=rhour(k1:k2);
    th(:,i)=thour(k1:k2);
end
%Extraction of radiation and temperature data per minute
load rminute;
load tminute;
rm=zeros(1440,365);
tm=zeros(1440,365);
for i=1:365
    k1=(i-1)*1440+1;
    k2=i*1440;
    rm(:,i)=rminute(k1:k2);
    tm(:,i)=tminute(k1:k2);
end
%Calculate the forecast data every 15 minutes
rmm=zeros(96,365);
tmm=zeros(96,365);
for i=1:96
    sumr=zeros(1,365);
    sumt=zeros(1,365);
    for j=1:15
        sumr(1,:)=sumr(1,:)+rm((i-1)*15+j,:);
        sumt(1,:)=sumt(1,:)+tm((i-1)*15+j,:);
    end
    rmm(i,:)=sumr(1,:)/15;
    tmm(i,:)=sumt(1,:)/15;
end

month_day=[31,28,31,30,31,30,31,31,30,31,30,31];
sumday=0;
for k=1:Month-1
    sumday=sumday+month_day(k);
end
sumday=sumday+Day;
RH=rh(:,sumday);
TH=th(:,sumday);
RMM=rmm(:,sumday);
TMM=tmm(:,sumday);
RM=rm(:,sumday);
TM=tm(:,sumday);
YH=zeros(1,24);
YMM=zeros(1,96);
YM=zeros(1,1440);
%Calculating 24-h-ahead forecast PV power
for k=1:24
    TH(k)=TH(k)+30*RH(k)/1000;
    YH(k)=RH(k)*Qpv/1000*(1-0.0047*(TH(k)-25));
end
%Calculating 15-min-ahead forecast PV power
for k=1:96
    TMM(k)=TMM(k)+30*RMM(k)/1000;
    YMM(k)=RMM(k)*Qpv/1000*(1-0.0047*(TMM(k)-25));
end
%Calculating real-time PV power
for k=1:1440
    TM(k)=TM(k)+30*RM(k)/1000;
    YM(k)=RM(k)*Qpv/1000*(1-0.0047*(TM(k)-25));
end

%interpolation of PV power
x=0.5:1:23.5;
u=0.25:0.25:24;
e=1/60:1/60:24;
yload=ones(1,1440);
ymload=ones(1,1440);
y=interp1(x,YH,e,'spline');
%% %%%%%%%%%%%%%%%%%%    System initialization   %%%%%%%%%%%%%%%%%%%
%Number of parameter adjustments
adjust=0;
%Indicate whether battery capacity is insufficient
normal_Q=0;
%Indicate whether battery power is insufficient
normal_P=0;
%Indicate whether this day is clear
clearday=0;
%Battery SOC
SOCM=zeros(1,1440);
for k=1:1:1440
    SOCM(k)=SOC0;
end
%Charging and Discharging Power of Batteries
BB=zeros(1,1440);
BBM=zeros(1,1440);
%Absorption power of electrolyzer
HH=zeros(1,1440);
HHM=zeros(1,1440);
%feed-in PV power
WW=zeros(1,1440);
%Power absorbed from the grid
MMM=zeros(1,1440);
%Abandoned PV power
FFM=zeros(1,1440);
%% %%%%%%%%%%%%%%%%%%    Day-ahead scheduling    %%%%%%%%%%%%%%%%%%%%
yr=y-yload;
time1=0;
time3=0;
for k=300:1:1440;
    if yr(k)>0
        time1=k+1;
        break
    end
end
for k=1300:-1:time1+1;
    if yr(k)>0
        time3=k+1;
        break
    end
end
if time1==0
    for k=1:1440
        MMM(k)=yload(k)-YM(k);
    end
else
clear yj;
len=time3-time1+1;
xx=[1/60:1/60:len/60]';
for k=time1:1:time3
    yj(k-time1+1)=yr(k);
end
yy=yj';
fun1=fittype(@(a1,b1,c1,x) a1*exp(-((x-b1)/c1).^2));
fun2=fittype(@(a2,b2,c2,x) a2*x.^2+b2*x+c2);
[f1,g1]=fit(xx,yy,fun1,'startpoint',[10000,8,4]);
[f2,g2]=fit(xx,yy,fun2,'startpoint',[-400,5000,-3000]);
if g1.rsquare>g2.rsquare
    z=@(x) f1.a1*exp(-((x-f1.b1)/f1.c1).^2);
    rsquare=g1.rsquare;
else
    z=@(x) f2.a2*x.^2+f2.b2*x+f2.c2;
    rsquare=g2.rsquare;
end
yrm=YM-ymload;
% clear day
if  rsquare>=square
    [f3,g3]=fit(e',yrm',fun1,'startpoint',[10000,8,4]);
    if g3.rmse/max(yr)<=sunday
        clearday=1;
    end
    if clearday==1
        kk=0;
    else
        if max(yr)<2000
            kk=0;
        else
            kk=0.32*max(yr);
        end
    end
% non-clear day
else
       if max(yr)<4000
            kk=0;
        else
            kk=0.32*max(yr);
        end
end
rest=zeros(1,1440);
for k=time1-1:time3+1
     WW(k)=yr(k)-kk;
     if WW(k)<0
         WW(k)=0;
     end
     rest(k)=yr(k)-WW(k);
end
%Allocate residual PV power to batteries and electrolyzers
if clearday==1
    for k=time1-1:1:time3+1
        BB(k)=rest(k);
    end
else 
   for k=time1-1:1:time3+1
        if rest(k)<PHmin
            BB(k)=rest(k);
            HH(k)=0;
        else
            BB(k)=0;
            HH(k)=rest(k);
        end
    end
end
%% %%%%%%%%%%%%%%%%%%     Real-time scheduling    %%%%%%%%%%%%%%%%%%%%
WWM=WW;
%%%%%%%%%%%%%%%%%%%%     Morning��07:00 to max{time1��time1m}��  %%%%%%%%%%%%%%%%%%%%

time1m=0;
time2m=0;
time3m=0;
time4m=0;
time5m=0;
for k=1:1:1440;
    if yrm(k)>0
        time1m=k;
        break
    end
end
if time1m==0
    for k=1:1440
        MMM(k)=yload(k)-YM(k);
    end
else
if time1<=7*60+1
    for k=1:1:min(time1m,time1)
        MMM(k)=MMM(k)+ymload(k);  
    end  
    if time1m<=time1
        for k=time1m:1:time1
            BBM(k)=yrm(k);
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
        end
    elseif time1m>time1       
        for k=time1:1:time1m
            BBM(k)=-WWM(k)-ymload(k)+YM(k);
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
        end              
    end
elseif time1>7*60+1 
    for k=1:1:7*60
        MMM(k)=MMM(k)+ymload(k);
    end  
    if time1m<=time1
        for k=7*60:1:time1m
            BBM(k)=-ymload(k)+YM(k);
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
            if SOCM(k)<SOCmin1
               time2m=k;
            break
            end
        end
        if time2m>0
            for k=time2m:1:time1m
                MMM(k)=ymload(k)-YM(k);
                SOCM(k)=SOCM(k-1);
                BBM(k)=0;
            end
        end
        for k=time1m:1:time1
            BBM(k)=yrm(k);
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
        end
    elseif time1m>time1
        for k=7*60:1:time1
            BBM(k)=-ymload(k)+YM(k);
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
            if SOCM(k)<SOCmin1
               time2m=k;
            break
            end
        end
        if time2m>0
            for k=time2m:1:time1
                MMM(k)=ymload(k)-YM(k);
                SOCM(k)=SOCM(k-1);
                BBM(k)=0;
            end
        end
        for k=time1:1:time1m
            BBM(k)=-WWM(k)-ymload(k)+YM(k);
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
        end              
    end
end
for k=time3-60:1:time3+60;
    if yrm(k)<0
        break;
    end
end
time3m=k;

%% %%%%%%%%%%%%%%%%%%   Daytime��time1 to time3��  %%%%%%%%%%%%%%%%%%%%
yrest=zeros(1,1440);
for k=max(time1m,time1)+1:1:min(time3m,time3)-1
    %15-min-ahead scheduling
    if mod(k,15)==0
        WWplan=0;
        loadplan=0;
        for i=1:15
            WWplan=WWplan+WWM(k+i);
            loadplan=loadplan+ymload(k+i);
        end
        WWplan=WWplan/15;
        loadplan=loadplan/15;
        if clearday==0
            if (YMM(k/15+1)-loadplan)-WWplan>PHmax
                add=(YMM(k/15+1)-loadplan)-WWplan-PHmax;
                for i=1:15
                    WWM(k+i-1)=WWM(k+i-1)+add;
                end
                adjust=adjust+15;
            elseif (YMM(k/15+1)-loadplan)-WWplan<-PBnormal
                add=(YMM(k/15+1)-loadplan)-WWplan;
                for i=1:15
                    WWM(k+i-1)=WWM(k+i-1)+add;
                end 
                adjust=adjust+15;
            end
        elseif clearday==1
            if (YMM(k/15+1)-loadplan)-WWplan>0.8*QBmax*Kmax  ||(YMM(k/15+1)-loadplan)-WWplan<-0.8*QBmax*Kmax  
                add=(YMM(k/15+1)-loadplan)-WWplan;
                for i=1:15
                    WWM(k+i-1)=WWM(k+i-1)+add;
                end
                adjust=adjust+15;
            end
        end
    end
    yrest(k)=yrm(k)-WWM(k);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     Scheme 1    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if SOCM(k-1)>SOCmin1 && SOCM(k-1)<SOCmax1
        if yrest(k)>=PHmin
            if clearday==0
                if HHM(k-1)==0 && SOCM(k-1)<SOCmax1 && yrest(k)<PBnormal
                    BBM(k)=yrest(k);
                    SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;%���
                else
                    HHM(k)=yrest(k);
                    BBM(k)=0;
                    SOCM(k)=SOCM(k-1);
                end
            else
                if yrest(k)<=Kmax*QBmax
                    BBM(k)=yrest(k);    
                else
                    BBM(k)=Kmax*QBmax;
                    FFM(k)=yrest(k)-Kmax*QBmax;  
                end
                 SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
            end
        elseif yrest(k)<PHmin && yrest(k)>0
            if HH(k)==0 || HHM(k-1)==0
                BBM(k)=yrest(k);
                SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1; 
            elseif HH(k)>0 && HHM(k-1)>0
                HHM(k)=PHmin;
                BBM(k)=yrest(k)-PHmin;
                SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;               
            end
        elseif yrest(k)<0 
            if abs(yrest(k))<=Kmax*QBmax
                BBM(k)=yrest(k);    
            else
                BBM(k)=-Kmax*QBmax;
                WWM(k)=WWM(k)-(-yrest(k)-Kmax*QBmax);  
                if WWM(k)<0
                    BBM(k)=BBM(k)+WWM(k);
                    WWM(k)=0;
                end
                adjust=adjust+1;
                normal_P=1;
            end
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     Scheme 2    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    elseif SOCM(k-1)>=SOCmax1
        if yrest(k)>=PHmin
            if clearday==0
                HHM(k)=yrest(k);
            else
                FFM(k)=yrest(k);
            end
            BBM(k)=0;
            SOCM(k)=SOCM(k-1);
        elseif yrest(k)<PHmin && yrest(k)>0
            if HH(k)==0 || HHM(k-1)==0
                FFM(k)=yrest(k);
                BBM(k)=0;
                SOCM(k)=SOCM(k-1);                  
            elseif HH(k)>0 && HHM(k-1)>0
                HHM(k)=PHmin;
                BBM(k)=yrest(k)-PHmin;
                SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
            end
        elseif yrest(k)<0        
            if abs(yrest(k))<=Kmax*QBmax
                BBM(k)=yrest(k);    
            else
                BBM(k)=-Kmax*QBmax;
                WWM(k)=WWM(k)-(-yrest(k)-Kmax*QBmax);  
                if WWM(k)<0
                    BBM(k)=BBM(k)+WWM(k);
                    WWM(k)=0;
                end
                adjust=adjust+1;
                normal_P=1;
            end
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
        end
    end
    if normal_Q==0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     Scheme 3    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if SOCM(k-1)<=SOCmin1
            if yrest(k)>0 && yrest(k)<=Kmax*QBmax
                if yrest(k)>PHmin && HHM(k-1)>0
                    HHM(k)=PHmin;
                    BBM(k)=yrest(k)-PHmin;
                else
                    BBM(k)=yrest(k);
                end
                SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
            elseif yrest(k)>Kmax*QBmax
                HHM(k)=QBmax;
                BBM(k)=yrest(k)-QBmax;
                SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;           
            elseif yrest(k)<0
                if yrest(k)>-2000 && SOCM(k-1)>SOCmin1
                    BBM(k)=yrest(k);
                    SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
                else
                    if yrm(k)>=PBnormal
                        BBM(k)=PBnormal;   
                        WWM(k)=WWM(k)-PBnormal+yrest(k);  
                        if WWM(k)<0
                            BBM(k)=BBM(k)+WWM(k);
                            WWM(k)=0;
                        end
                        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;                
                        adjust=adjust+1;
                    elseif yrm(k)<PBnormal && WWM(k)>0
                        BBM(k)=yrm(k);
                        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
                        WWM(k)=0;
                        adjust=adjust+1;
                    elseif WWM==0
                        MMM(k)=-yrm(k)+PBnormal;
                        BBM(k)=PBnormal;
                        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;     
                    end
                end
            end
            normal_Q=1;
        end
    elseif normal_Q~=0
        if SOCM(k-1)<=SOCmin2
            if yrest(k)>0 && yrest(k)<=Kmax*QBmax
                if yrest(k)>PHmin && HHM(k-1)>0
                    HHM(k)=PHmin;
                    BBM(k)=yrest(k)-PHmin;
                elseif yrest(k)<=PHmin || HHM(k-1)==0
                    BBM(k)=yrest(k);
                end
                SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
            elseif yrest(k)>Kmax*QBmax
                HHM(k)=QBmax;
                BBM(k)=yrest(k)-QBmax;
                SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;         
            elseif yrest(k)<0
                if yrest(k)>-2000 && SOCM(k-1)>SOCmin1
                    BBM(k)=yrest(k);
                    SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
                else
                    if yrm(k)>=PBnormal
                        BBM(k)=PBnormal;   
                        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
                        WWM(k)=WWM(k)-PBnormal+yrest(k);
                        if WWM(k)<0
                            BBM(k)=BBM(k)+WWM(k);
                            WWM(k)=0;
                        end
                        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;                
                        adjust=adjust+1;
                    elseif yrm(k)<PBnormal && WWM(k)>0
                        BBM(k)=yrm(k);
                        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
                        WWM(k)=0;
                        adjust=adjust+1;
                    elseif WWM==0
                        MMM(k)=-yrm(k)+PBnormal;
                        BBM(k)=PBnormal;
                        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;     
                    end
                end
            end
            if SOCM(k)>SOCmin2
                normal_Q=0;
            end
        end
    end   
    if HHM(k)>PHmax
        FFM(k)=HHM(k)-PHmax;
        HHM(k)=PHmax;     
    end
end
%% %%%%%%%%%%%%%%%%%%%%   Evening��min{time3m��time3} to 23:00��  %%%%%%%%%%%%%%%%%%%%
if time3<=time3m
    for k=time3:1:time3m
        BBM(k)=yrm(k);
        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
    end  
elseif time3>time3m
    for k=time3m:1:time3
        BBM(k)=-WWM(k)+yrm(k);
        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
    end
end
if max(time3m,time3)<18*60
    for k=max(time3m,time3)+1:1:18*60
        MMM(k)=ymload(k)-YM(k);
        SOCM(k)=SOCM(k-1);
    end
    if SOCM(k)>SOCmin1
        for k=18*60+1:1:23*60+1
            BBM(k)=YM(k)-ymload(k);
            SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
            if SOCM(k)<SOCmin1
                 time4m=k;
                 break
             end
        end
    else time4m=18*60;
    end
else
    if SOCM(max(time3m,time3))>SOCmin1
    for k=max(time3m,time3)+1:1:23*60
         BBM(k)=YM(k)-ymload(k);
         SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax/KB2;
         if SOCM(k)<SOCmin1
             time4m=k;
             break
         end
    end
    else time4m=time3m;
    end
end
if time4m>0
    for k=time4m:1:23*60
        MMM(k)=ymload(k)-YM(k);
        BBM(k)=YM(k);
        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
    end
end
%% %%%%%%%%%%%%%%%%%%    Night��23:00 to 07:00 in the next day��   %%%%%%%%%%%%%%%%%%%%

Hday=trapz(e,HHM)/Qh1;
if Hday<Havr
    Hnight=Havr-Hday;
else
    Hnight=0;
end
tH=round(Hnight*Qh1/PHmax*60);
if tH>60
    for k=23*60+1:1:1440
        MMM(k)=MMM(k)+PHmax;
        HHM(k)=PHmax;
    end
    for k=1:1:tH-60
        MMM(k)=MMM(k)+PHmax;
        HHM(k)=PHmax;
    end
else 
    for k=23*60+1:1:23*60+tH
        MMM(k)=MMM(k)+PHmax;
        HHM(k)=PHmax;
    end
end
for k=23*60+2:1:1440
    MMM(k)=MMM(k)+ymload(k);
    SOCM(k)=SOCM(k-1);
end
for k=23*60:1:1440
    SOCM(k)=SOCM(k-1);
end
if SOCM(1440)<SOCmin1
    SOCM(1)=SOCmin1;
else SOCM(1)=SOCM(1440);
end
if SOCM(1)<SOC0
    for k=2:1:7*60
        MMM(k)=MMM(k)+PBnormal;
        BBM(k)=PBnormal;
        SOCM(k)=SOCM(k-1)+BBM(k)*1/60/QBmax*KB1;
        if SOCM(k)>SOC0
            time5m=k;
            break
        end
    end
    for k=time5m+1:1:7*60
        SOCM(k)=SOCM(time5m);
    end
else
    for  k=1:7*60
        SOCM(k)=SOC0;
    end
end
end
end
HHMM=smooth(HHM,10);
BBMM=BBM+(HHMM'-HHM);
%% %%%%%%%%%%%%%%%%%%%%  Economic evaluation  %%%%%%%%%%%%%%%%%%%%

buy_peak=0;buy_mean=0;buy_valley=0;
for k=8*60+1:1:12*60
    buy_peak=buy_peak+MMM(k)*1/60;
end
for k=18*60+1:1:23*60
    buy_peak=buy_peak+MMM(k)*1/60;
end
for k=7*60+1:1:8*60
    buy_mean=buy_mean+MMM(k)*1/60;
end
for k=12*60+1:1:18*60
    buy_mean=buy_mean+MMM(k)*1/60;
end
for k=23*60:1:1440
    buy_valley=buy_valley+MMM(k)*1/60;
end
for k=1:1:7*60
    buy_valley=buy_valley+MMM(k)*1/60;
end
E_expend=buy_peak*price_peak+buy_mean*price_mean+buy_valley*price_valley;
if time1==0
    E1_income=0;
else
    ycen=Qcen/Qpv*YM;
    ydis=Qdis/Qpv*YM;
    ydis_rest=zeros(1,1440);
    ycen_rest=zeros(1,1440);
    for k=1:1:time1-1
        ydis_rest(k)=0;
        ycen_rest(k)=0;
    end
    for k=time1:1:time3
        if ydis(k)>ymload(k)
            ydis_rest(k)=ydis(k)-ymload(k);
            ycen_rest(k)=ycen(k);
        else
            ydis_rest(k)=0;
            ycen_rest(k)=ycen(k)-(ymload(k)-ydis(k));
        end
    end
    for k=time3+1:1:1440
        ydis_rest(k)=0;
        ycen_rest(k)=0;
    end
    Wdis=zeros(1,1440);
    Wcen=zeros(1,1440);
    for k=time1:1:time3
        Wdis(k)=ydis_rest(k)/(ydis_rest(k)+ycen_rest(k))*WWM(k);
        Wcen(k)=WWM(k)-Wdis(k);
    end
    Edis_income=trapz(e,Wdis)*price_dis;
    Ecen_income=trapz(e,Wcen)*price_cen;
    E1_income=Edis_income+Ecen_income;
end
sell_peak=0;sell_mean=0;sell_valley=0;
ylocal=zeros(1,1440);
for k=7*60:23*60
    ylocal(k)=ymload(k)-MMM(k);
end
for k=8*60+1:1:12*60
    sell_peak=sell_peak+ylocal(k)*1/60;
end
for k=18*60+1:1:23*60
    sell_peak=sell_peak+ylocal(k)*1/60;
end
for k=7*60+1:1:8*60
    sell_mean=sell_mean+ylocal(k)*1/60;
end
for k=12*60+1:1:18*60
    sell_mean=sell_mean+ylocal(k)*1/60;
end
E2_income=sell_peak*price_peak+sell_mean*price_mean;
S_income=trapz(e,ydis)*subsidy;
H_income=(Hday+Hnight)*price_H;
total_income=E1_income+E2_income+H_income+S_income;
total_expend=E_expend;
profit=total_income-total_expend;
%% %%%%%%%%%%%%%%%%%%%%     Draw pictures    %%%%%%%%%%%%%%%%%%%%
s_f=20;s_xy=15;w_f=2.5;w_axi=2;

figure();
subplot(2,2,1)
h1=plot(e,WW/1000);
ht1=title('(a)');
hx1=xlabel('Time of the day');
hy1=ylabel('Power (MW)');
hl1=legend('P_{fin}^{sche}');
axis([5,20,-6,20]);
ha1=gca;
set(ha1,'box','off')

subplot(2,2,2)
h2=plot(e,WWM/1000);
ht2=title('(b)');
hx2=xlabel('Time of the day');
hy2=ylabel('Power (MW)');
hl2=legend('P_{fin}^{act}');
axis([5,20,-6,20]);
ha2=gca;
set(ha2,'box','off')

subplot(2,2,3)
h3=plot(e,BBMM/1000);
ht3=title('(c)');
hx3=xlabel('Time of the day');
hy3=ylabel('Power (MW)');
hl3=legend('P_B');
axis([5,20,-6,20]);
ha3=gca;
set(ha3,'box','off')

subplot(2,2,4)
h4=plot(e,HHMM/1000);
ht4=title('(d)');
hx4=xlabel('Time of the day');
hy4=ylabel('Power (MW)');
hl4=legend('P_E');
axis([5,20,-6,20]);
ha4=gca;
set(ha4,'box','off')

hh=[h1,h2,h3,h4];
ht=[ht1,ht2,ht3,ht4];
hx=[hx1,hx2,hx3,hx4];
hy=[hy1,hy2,hy3,hy4];
ha=[ha1,ha2,ha3,ha4];
hl=[hl1,hl2,hl3,hl4];
set(hl,'Box','off');
set(hh,'Linewidth',w_f);
set([hx hy ht],'Fontsize',s_xy,'FontWeight','bold');
set(ha,'Linewidth',w_axi,'Fontsize',s_f,'FontWeight','bold');

%% %%%%%%%%%%%%%%%%%%     Calculating Battery Cycle Parameters    %%%%%%%%%%%%%%%%%%%%

N=0;
tp=sig2ext(SOCM);
rf=rainflow(tp);
DOD=2*rf(1,:);
Nctf=@(DOD) 1567*DOD.^(-1.157)+1076;
kc=length(DOD);
for k=1:kc
   if  rf(3,k)==1
       N=N+Nctf(1)/Nctf(DOD(k));
   else
       N=N+0.5*Nctf(1)/Nctf(DOD(k));
   end
end
T=Nctf(1)/N;

aaa=0;
 for i=time1:time3 
     aaa=aaa+abs(BBM(i)); 
 end
 aaa=aaa/(time3-time1);